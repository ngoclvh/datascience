import numpy as np
import random
n1 = input("nhap so N de tao ma tran NxNxN: ")
n = int(n1)
l = []
while len(l) in range((n*n*n)):
    l.append(round(random.uniform(0, 10),2))
print(l)
a = np.array(l).reshape((n,n,n))
print(a)
print('Min chieu sau: ', a.min(axis=0))
print('Max chieu sau: ', a.max(axis=0))
print('Sum chieu sau: ', a.sum(axis=0))
print('Min chieu cao: ', a.min(axis=1))
print('Max chieu cao: ', a.max(axis=1))
print('Sum chieu cao: ', a.sum(axis=1))
print('Min chieu rong: ', a.min(axis=2))
print('Max chieu rong: ', a.max(axis=2))
print('Sum chieu rong: ', a.sum(axis=2))