import numpy as np
import random
i = input("nhap so M,N,K de tao ma tran MxNxK: ")
l = [int(x) for x in i.split(',')]
print(l)
num = l[0]*l[1]*l[2]
list_num = []
while len(list_num) in range(num):
    list_num.append(random.randint(-100, 100))
a = np.array(list_num).reshape((l[0],l[1],l[2]))
print(a)
print('Min chieu sau: ', a.min(axis=0))
print('Max chieu sau: ', a.max(axis=0))
print('Sum chieu sau: ', a.sum(axis=0))
print('Min chieu cao: ', a.min(axis=1))
print('Max chieu cao: ', a.max(axis=1))
print('Sum chieu cao: ', a.sum(axis=1))
print('Min chieu rong: ', a.min(axis=2))
print('Max chieu rong: ', a.max(axis=2))
print('Sum chieu rong: ', a.sum(axis=2))

